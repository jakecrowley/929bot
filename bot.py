#!/user/bin/python3.7

import discord
import secrets # File that stores login information for the bot
import shelve
import datetime
import asyncio
from dateutil import tz

nine29ners = {}

def save(key, data):
    with shelve.open("929data") as db:
        db[key] = data

currentlist = []
pastlist = []

triggertime = [9, 29] #for debugging

utc = tz.gettz('UTC')
est = tz.gettz('America/New_York')

itis929 = False
async def isit929():
    global itis929
    global pastlist

    while True:
        if (datetime.datetime.now().hour in (triggertime[0], triggertime[0] + 12)) and (datetime.datetime.now().minute == triggertime[1]):
            print("[DEBUG] It is 929! ")

            itis929 = True
            while (datetime.datetime.now().hour in (triggertime[0], triggertime[0] + 12)) and (datetime.datetime.now().minute == triggertime[1]):
                await asyncio.sleep(0.1)
            print("[DEBUG] It is no longer 929!")
            itis929 = False
            
            pastlist = currentlist.copy() #copies currentlist to pastlist and clears currentlist after 929
            currentlist.clear()
            save("pastlist", pastlist)

            guild = client.get_guild(377637608848883723);
            printuserlist(pastlist)
            for id_code in nine29ners:
                if id_code not in pastlist:
                    member = guild.get_member(id_code)
                    print("[DEBUG] " + (member.nick, member.name)[member.nick is None] + " did not participate, resetting streak.")
                    nine29ners[id_code].currentstreak = 0
            save("929rs", nine29ners)
            
            await client.recap()
            await asyncio.sleep(11*60*60 + 58*60 + 30) # Sleeps for 11 hours 58 minutes and 30 seconds.
        else:
            await asyncio.sleep(0.1)


class nine29er:
    def __init__(self, id_code):
        self.currentstreak = 0
        self.points = 0
        self.maxstreak = 0
        self.count = 0
        self.id_code = id_code

    def trigger(self, first=False):
        if self.id_code in pastlist:
            self.currentstreak += 1
            print("[DEBUG] In pastlist, New currentstreak = " + str(self.currentstreak))
        else:
            self.currentstreak = 1
            print("[DEBUG] Not in pastlist, New currentstreak = " + str(self.currentstreak))

        if self.currentstreak > self.maxstreak:
            self.maxstreak = self.currentstreak

        if first:
            self.points += 1.5*(1 + int(self.currentstreak/5))
        else:
            self.points += (1 + int(self.currentstreak/5))

        self.count += 1

async def process929(author929):
    if author929.id not in nine29ners:
        nine29ners[author929.id] = nine29er(author929.id)

    if author929.id not in currentlist:
        print("[DEBUG] " + (author929.nick, author929.name)[author929.nick is None] + " did a 929!")

        currentlist.append(author929.id)
        nine29ners[author929.id].trigger(len(currentlist)==1)
        #save("929rs", nine29ners)

class bot929(discord.Client):
    async def on_ready(self):
        print('Logged on as', self.user)
            
    async def recap(self):
        pass

    async def on_message(self, message):
        created_at = message.created_at.replace(tzinfo=utc).astimezone(est)

        if message.author == self.user:
            return
        if message.content == '!ping':
            await message.channel.send('pong!')
        if (created_at.hour in (triggertime[0], triggertime[0] + 12)) and (created_at.minute == triggertime[1]):
            if (message.content == "929") and (message.guild.id == 377637608848883723):
                await process929(message.author)
        if message.content.lower() == "!leaderboard":
            msg = ""
            counter = 1
            for key in sorted(nine29ners, key = lambda id_code: nine29ners[id_code].points, reverse=True): #sorts 929ers by points
                member = message.guild.get_member(nine29ners[key].id_code)
                msg = msg + str(counter) + ". " + ((member.nick, member.name)[member.nick is None]).replace("|", "\\|") + ": " + str(nine29ners[key].points) + "\n"
                counter += 1
            await message.channel.send(embed=discord.Embed(title="Leaderboard", description=msg, color=0x992D22))
        if message.content.split(" ")[0] == "!profile":
            member = message.author
            if len(message.mentions) == 1:
                member = message.mentions[0]

            if member.id in nine29ners:
                n29er = nine29ners[member.id]

                embed = discord.Embed(title="Profile for " + (member.nick, member.name)[member.nick is None], color=0x992D22)
                embed.add_field(name="Current Streak", value=str(n29er.currentstreak), inline=False)
                embed.add_field(name="Longest Streak", value=str(n29er.maxstreak), inline=False)
                embed.add_field(name="Total Points", value=str(n29er.points), inline=False)
                embed.add_field(name="Total 929s", value=str(n29er.count), inline=False)
                await message.channel.send(embed=embed)
            else:
                await message.channel.send('You have not participated in a 9:29 yet!')
        #if message.content.lower() == "!debug":
            #guild = client.get_guild(377637608848883723);
            #printuserlist(pastlist)
            #for id_code in nine29ners:
            #    if id_code not in pastlist:
            #        member = guild.get_member(id_code)
            #        print("[DEBUG] " + (member.nick, member.name)[member.nick is None] + " did not participate, resetting streak.")
            #        nine29ners[id_code].currentstreak = 0   

def printuserlist(list):
    output = "["
    guild = client.get_guild(377637608848883723);
    for id_code in list:
        member = guild.get_member(id_code)
        output = output + (member.nick, member.name)[member.nick is None] + ", "
    print(output[:-2] + "]")

with shelve.open("929data") as db:
    if "929rs" in db:
        nine29ners = db["929rs"]
    else:
        print("929rs data not found using empty dictionary.")

    if "pastlist" in db:
        pastlist = db["pastlist"]
    else:
        print("pastlist data not found using empty list.")

client = bot929()
client.loop.create_task(isit929())
client.run(secrets.client_token)
